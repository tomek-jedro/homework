package com.homework.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BookServiceTest {

    @Autowired
    private BookService bookService;

    @Test
    public void bookByIsbn(){
        assertTrue(bookService.bookByIsbn("9781592432172").getTitle().equals("A Hypervista of the Java Landscape"));
        assertTrue(bookService.bookByIsbn("9780071606325").getPublisher().equals("McGraw Hill Professional"));
        assertTrue(bookService.bookByIsbn("N1IiAQAAIAAJ").getSubtitle().equals("First Contact"));
    }

}