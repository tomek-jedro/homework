package com.homework.services;

import com.homework.result.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthorRatingsServiceTest {

    @Autowired
    private AuthorRatingsService authorRatingsService;


    @Test
    public void authorRatings(){
        List<Author> authors = authorRatingsService.authorRatings();

        assertEquals(authors.get(0).getAverageRating(), 4.3);
        assertEquals(authors.get(4).getAverageRating(), 4.3);
        assertEquals(authors.get(6).getAverageRating(), 4.0);
    }

}