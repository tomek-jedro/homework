package com.homework.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryServiceTest {

    @Autowired
    private CategoryService categoryService;

    @Test
    public void booksByCategory(){
        assertTrue(categoryService.booksByCategory("nonexisting").size() == 0);
        assertTrue(categoryService.booksByCategory("Java").size() == 2);
        assertTrue(categoryService.booksByCategory("java").size() == 0);
    }

}