package com.homework.services;

import com.homework.book.Book;
import com.homework.components.BookLoader;
import com.homework.result.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class AuthorRatingsService {

    @Autowired
    private BookLoader bookLoader;

    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public List<Author> authorRatings(){
        List<String> namesOfAuthors = new ArrayList<>();
        List<Author> authors = new ArrayList<>();
        for (Book book : bookLoader.getBooks()) {
            for (String currentNameOfAuthor : book.getVolumeInfo().getAuthors()) {
                namesOfAuthors.add(currentNameOfAuthor);
            }
        }
        int booksWrittenByAuthor = 0;
        double amountOfAuthorRatings = 0;
        for (String currentAuthor : namesOfAuthors){
            for (Book book : bookLoader.getBooks()){
                for (String currentAuthorOfBook : book.getVolumeInfo().getAuthors()){
                    if (currentAuthor.equals(currentAuthorOfBook)){
                        if (book.getVolumeInfo().getAverageRating() != null){
                            booksWrittenByAuthor++;
                            amountOfAuthorRatings = amountOfAuthorRatings + book.getVolumeInfo().getAverageRating();
                        }
                    }
                }
            }
            if (booksWrittenByAuthor > 0){
                Author author = new Author();
                author.setAuthor(currentAuthor);
                author.setAverageRating(round(amountOfAuthorRatings / booksWrittenByAuthor, 1));
                authors.add(author);
            }
        }
        authors.sort(Comparator.comparing(Author::getAverageRating).reversed());
        return authors;
    }
}
