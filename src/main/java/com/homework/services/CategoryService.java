package com.homework.services;

import com.homework.book.Book;
import com.homework.book.parts.IndustryIdentifiers;
import com.homework.components.BookLoader;
import com.homework.components.DateTwoFormatsParser;
import com.homework.result.book.BookByCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CategoryService {

    @Autowired
    private BookLoader bookLoader;

    @Autowired
    private DateTwoFormatsParser dateTwoFormatsParser;

    public List<BookByCategory> booksByCategory(String categoryName){
        List<BookByCategory> booksWithCorrectCategoryName = new ArrayList<>();
        for (Book currentBook : bookLoader.getBooks()){
            for (String currentCategory : currentBook.getVolumeInfo().getCategories()){
                if (categoryName.equals(currentCategory)){
                    BookByCategory bookByCategory = new BookByCategory();
                    for (IndustryIdentifiers currentID: currentBook.getVolumeInfo().getIndustryIdentifiers()){
                        if (currentID.getType().equals("ISBN_13")){
                            bookByCategory.setIsbn(currentID.getIdentifier());
                        }
                    }
                    if (bookByCategory.getIsbn() == null){
                        bookByCategory.setIsbn(currentBook.getId());
                    }
                    bookByCategory.setTitle(currentBook.getVolumeInfo().getTitle());
                    bookByCategory.setPublisher(currentBook.getVolumeInfo().getPublisher());
                    if(currentBook.getVolumeInfo().getPublishedDate().length() > 0){
                        Date date = dateTwoFormatsParser.tryParse(currentBook.getVolumeInfo().getPublishedDate());
                        bookByCategory.setPublishedDate(date.getTime());
                    }
                    bookByCategory.setDescription(currentBook.getVolumeInfo().getDescription());
                    bookByCategory.setPageCount(currentBook.getVolumeInfo().getPageCount());
                    bookByCategory.setThumbnailUrl(currentBook.getVolumeInfo().getImageLinks().getThumbnail());
                    bookByCategory.setLanguage(currentBook.getVolumeInfo().getLanguage());
                    bookByCategory.setPreviewLink(currentBook.getVolumeInfo().getPreviewLink());
                    bookByCategory.setAuthors(currentBook.getVolumeInfo().getAuthors());
                    bookByCategory.setCategories(currentBook.getVolumeInfo().getCategories());
                    booksWithCorrectCategoryName.add(bookByCategory);
                }
            }
        }
        return booksWithCorrectCategoryName;
    }
}
