package com.homework.services;


import com.homework.NotFoundException;
import com.homework.book.Book;
import com.homework.book.parts.IndustryIdentifiers;
import com.homework.components.BookLoader;
import com.homework.components.DateTwoFormatsParser;
import com.homework.result.book.BookResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class BookService {

    @Autowired
    private BookLoader bookLoader;

    @Autowired
    private DateTwoFormatsParser dateTwoFormatsParser;

    public BookResult bookByIsbn(String isbn){
        BookResult bookResult = new BookResult();
        for (Book currentBook: bookLoader.getBooks()){
            for (IndustryIdentifiers currentID: currentBook.getVolumeInfo().getIndustryIdentifiers()){
                if (currentID.getType().equals("ISBN_13") && currentID.getIdentifier().equals(isbn) || currentBook.getId().equals(isbn)) {
                    bookResult.setIsbn(isbn);
                    bookResult.setTitle(currentBook.getVolumeInfo().getTitle());
                    bookResult.setSubtitle(currentBook.getVolumeInfo().getSubtitle());
                    bookResult.setPublisher(currentBook.getVolumeInfo().getPublisher());

                    if(currentBook.getVolumeInfo().getPublishedDate().length() > 0){
                        Date date = dateTwoFormatsParser.tryParse(currentBook.getVolumeInfo().getPublishedDate());

                        bookResult.setPublishedDate(date.getTime());
                    }

                    bookResult.setDescription(currentBook.getVolumeInfo().getDescription());
                    bookResult.setPageCount(currentBook.getVolumeInfo().getPageCount());
                    bookResult.setThumbnailUrl(currentBook.getVolumeInfo().getImageLinks().getThumbnail());
                    bookResult.setLanguage(currentBook.getVolumeInfo().getLanguage());
                    bookResult.setPreviewLink(currentBook.getVolumeInfo().getPreviewLink());
                    bookResult.setAverageRating(currentBook.getVolumeInfo().getAverageRating());
                    bookResult.setAuthors(currentBook.getVolumeInfo().getAuthors());
                    bookResult.setCategories(currentBook.getVolumeInfo().getCategories());
                    return bookResult;
                }
            }
        }
        throw new NotFoundException("No results found");

    }
}
