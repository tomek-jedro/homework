package com.homework.components;

import com.google.gson.Gson;
import com.homework.book.Book;
import com.homework.book.parts.RestResponse;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;

@Component
@Getter
public class BookLoader {

    private List<Book> books;

    @Value("${homework.fileName}")
    private String filename;

    @PostConstruct
    public void returnBook(){
        Gson gson = new Gson();
        try {
            File file = new File(filename);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            RestResponse restResponse = gson.fromJson(fileReader, RestResponse.class);
            bufferedReader.close();
            books = restResponse.getItems();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
