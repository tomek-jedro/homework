package com.homework.components;


import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class DateTwoFormatsParser {

    List<String> formatStrings = Arrays.asList("yyyy-MM-dd", "yyyy");

    public Date tryParse(String dateString)
    {
        for (String formatString : formatStrings)
        {
            try
            {
                return new SimpleDateFormat(formatString).parse(dateString);
            }
            catch (ParseException e) {}
        }

        return null;
    }

}
