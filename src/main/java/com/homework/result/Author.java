package com.homework.result;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Author {
    private String author;
    private double averageRating;
}
