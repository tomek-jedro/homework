package com.homework.book.parts;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class VolumeInfo {
    private String title;
    private String subtitle;
    private String publisher;
    private ImageLinks imageLinks;
    private String language;
    private String previewLink;
    private List<String> authors = new ArrayList<>();
    private List<String> categories = new ArrayList<>();
    private int pageCount;
    private String description;
    private String publishedDate = "";
    private Double averageRating;
    private List<IndustryIdentifiers> industryIdentifiers;

    }
