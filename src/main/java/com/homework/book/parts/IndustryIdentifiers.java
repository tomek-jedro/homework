package com.homework.book.parts;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IndustryIdentifiers {
    private String type;
    private String identifier;
}
