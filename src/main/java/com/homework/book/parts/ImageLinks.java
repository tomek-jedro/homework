package com.homework.book.parts;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageLinks {
    private String thumbnail;
}
