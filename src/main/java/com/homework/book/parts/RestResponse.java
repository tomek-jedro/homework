package com.homework.book.parts;


import com.homework.book.Book;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RestResponse {
    private List<Book> items;

}
