package com.homework.book;

import com.homework.book.parts.VolumeInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book {
    private String id;
    private VolumeInfo volumeInfo;
}
