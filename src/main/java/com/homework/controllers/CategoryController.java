package com.homework.controllers;

import com.google.gson.Gson;
import com.homework.components.BookLoader;
import com.homework.components.DateTwoFormatsParser;
import com.homework.result.book.BookByCategory;
import com.homework.book.Book;
import com.homework.book.parts.IndustryIdentifiers;
import com.homework.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/category/{categoryName}/books")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public List<BookByCategory> categoryBooks(@PathVariable("categoryName") String categoryName){
        return categoryService.booksByCategory(categoryName);
    }
}
