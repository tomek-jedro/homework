package com.homework.controllers;

import com.google.gson.Gson;
import com.homework.result.Author;
import com.homework.components.BookLoader;
import com.homework.book.Book;
import com.homework.services.AuthorRatingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/rating")
public class AuthorRatingsController {

    @Autowired
    private AuthorRatingsService authorRatingsService;

    @GetMapping
    public List<Author> rating(){
        return authorRatingsService.authorRatings();
    }
}
