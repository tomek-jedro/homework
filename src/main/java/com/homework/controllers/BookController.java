package com.homework.controllers;

import com.google.gson.Gson;
import com.homework.components.BookLoader;
import com.homework.components.DateTwoFormatsParser;
import com.homework.NotFoundException;
import com.homework.result.book.BookResult;
import com.homework.book.Book;
import com.homework.book.parts.IndustryIdentifiers;
import com.homework.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/book/{isbn}")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public BookResult bookView(@PathVariable("isbn") String isbn){
        return bookService.bookByIsbn(isbn);
    }

}
